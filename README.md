# Portfolio
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![Security Rating](https://sonarqube.loicbertrand.net/api/project_badges/measure?project=portfolio&metric=security_rating)](https://sonarqube.loicbertrand.net/dashboard?id=portfolio)


This project is for me not only a portfolio but also a practical application of my CI/CD skills.

I set up a Gitlab-CI pipeline which is described in the .gitlab-ci file. It allows me to automate the actions below :

1. Source code is analyzed by my SonarQube instance which is hosted at https://sonarqube.loicbertrand.net

2. Application is built in an Node.js container

3. Application is deployed in a Nginx container based on the previous build.