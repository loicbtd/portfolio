import 'bootswatch/dist/lux/bootstrap.min.css'

import React, { Component } from 'react'

import { Carousel } from 'react-bootstrap'
import knowledge_framework from 'resources/images/knowledge-framework.webp'
import knowledge_languages from 'resources/images/knowledge-languages.webp'
import knowledge_os from 'resources/images/knowledge-os.webp'
import knowledge_system from 'resources/images/knowledge-system.webp'
import knowledge_tools from 'resources/images/knowledge-tools.webp'
import { withTranslation } from 'react-i18next'

class KnowledgesScreen extends Component {
  render() {
    const t = this.props.t
    return (
      <Carousel
        interval={5000}
        className='screen vertical-center background-desk'>
        <div className='carousel-item'>
          <div className='container my-5'>
            <div className='row text-center'>
              <div className='col-12'>
                <h1>{t('knowledges.frameworks.title')}</h1>
              </div>
              <div className='col-12'>
                <img
                  src={knowledge_framework}
                  alt='Framework knowledges'
                  className='img-fluid img-knowledge'
                />
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container my-5'>
            <div className='row text-center'>
              <div className='col-12'>
                <h1>{t('knowledges.languages.title')}</h1>
              </div>
              <div className='col-12'>
                <img
                  src={knowledge_languages}
                  alt='Languages knowledges'
                  className='img-fluid img-knowledge'
                />
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container my-5'>
            <div className='row text-center'>
              <div className='col-12'>
                <h1>{t('knowledges.os.title')}</h1>
              </div>
              <div className='col-12'>
                <img
                  src={knowledge_os}
                  alt='Operating system knowledges'
                  className='img-fluid img-knowledge'
                />
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container my-5'>
            <div className='row text-center'>
              <div className='col-12'>
                <h1>{t('knowledges.system.title')}</h1>
              </div>
              <div className='col-12'>
                <img
                  src={knowledge_system}
                  alt='System knowledges'
                  className='img-fluid img-knowledge'
                />
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container my-5'>
            <div className='row text-center'>
              <div className='col-12'>
                <h1>{t('knowledges.tools.title')}</h1>
              </div>
              <div className='col-12'>
                <img
                  src={knowledge_tools}
                  alt='Development tools knowledges'
                  className='img-fluid img-knowledge'
                />
              </div>
            </div>
          </div>
        </div>
      </Carousel>
    )
  }
}

export default withTranslation()(KnowledgesScreen)
