import 'bootswatch/dist/lux/bootstrap.min.css'

import React, { Component } from 'react'

import { Carousel } from 'react-bootstrap'
import illustration_handshake from 'resources/images/illustration-handshake.webp'
import illustration_studies from 'resources/images/illustration-studies.webp'
import profile_loic from 'resources/images/profile-loic.webp'
import { withTranslation } from 'react-i18next'

class MeScreen extends Component {
  render() {
    const t = this.props.t
    return (
      <Carousel interval={5000} className='background-worldmap screen'>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='profile-picture img-not-draggable'
                  src={profile_loic}
                  alt='Loïc BERTRAND profile'
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h2 text-light'>{t('me.slide1.title')}</h1>
                <h2 className='h4 text-light'>{t('me.slide1.hook')}</h2>
                <p className='slide-text-content text-light'>
                  {t('me.slide1.content')}
                </p>
                <a
                  href='https://repo.loicbertrand.net/share/cv-loicbertrand.pdf'
                  target='_blank'
                  rel='noopener noreferrer'
                  className='btn btn-secondary m-1'>
                  {t('me.extra.resume')}
                </a>
                <a
                  href='https://repo.loicbertrand.net/share/details-formation.pdf'
                  target='_blank'
                  rel='noopener noreferrer'
                  className='btn btn-secondary m-1'>
                  {t('me.extra.courses')}
                </a>
                <a
                  href='https://repo.loicbertrand.net/share/planning-formation.pdf'
                  target='_blank'
                  rel='noopener noreferrer'
                  className='btn btn-secondary m-1'>
                  {t('me.extra.planning')}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='profile-picture img-not-draggable'
                  src={illustration_handshake}
                  alt='handshake'
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h3 text-light'>{t('me.slide2.title')}</h1>
                <h2 className='h5 text-light'>{t('me.slide2.hook')}</h2>
                <p className='slide-text-content text-light'>
                  {t('me.slide2.content')}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='profile-picture img-not-draggable'
                  src={illustration_studies}
                  alt='studies'
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h4 text-light'>{t('me.slide3.title')}</h1>
                <h2 className='h6 text-light'>{t('me.slide3.hook')}</h2>
                <p className='slide-text-content text-light'>
                  {t('me.slide3.content')}
                </p>
              </div>
            </div>
          </div>
        </div>
      </Carousel>
    )
  }
}

export default withTranslation()(MeScreen)
