import 'bootswatch/dist/lux/bootstrap.min.css'
import 'resources/style/phoneinput/bootswatch-lux.css'

import * as Icons from '@fortawesome/free-solid-svg-icons'
import * as yup from 'yup'

import { Controller, useForm } from 'react-hook-form'
import React, { useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PhoneInput from 'react-phone-input-2'
import emailjs from 'emailjs-com'
import { useTranslation } from 'react-i18next'

function ContactScreen(props) {
  
  const t = useTranslation().t

  const validationSchema = yup.object().shape({
    name: yup
      .string(t('contact.form.name.error.string'))
      .required(t('contact.form.name.error.required')),
    email: yup
      .string(t('contact.form.email.error.string'))
      .email(t('contact.form.email.error.email'))
      .required(t('contact.form.email.error.required')),
    phone: yup.number(t('contact.form.error.number')),
    message: yup
      .string(t('contact.form.message.error.string'))
      .required(t('contact.form.message.error.required')),
  })

  const { register, handleSubmit, errors, formState, control } = useForm({
    mode: 'onBlur',
    validationSchema: validationSchema,
    validateCriteriaMode: 'all',
  })

  const [formStatus, setFormStatus] = useState(null)
  
  const handleAfterSubmit = (data) => {
    const templateParams = {
      name: data.name,
      email: data.email,
      phone: data.phone ? data.phone : '',
      message: data.message
    }

    emailjs
      .send(
        process.env.REACT_APP_EMAILJS_SERVICEID,
        process.env.REACT_APP_EMAILJS_TEMPLATEID,
        templateParams,
        process.env.REACT_APP_EMAILJS_UID
      )
      .then(
        (result) => {
          setFormStatus('success')
        },
        (error) => {
          setFormStatus('error')
        }
      )
  }  

  const handleBackButtonClick = () => {
    setFormStatus(null)
    props.parent.scrollToTop()
  }

  const contactFormSubmitSuccess = (
    <div className='alert alert-dismissible alert-secondary'>
      <p className='h5'>{t('contact.form.submit.feedback.success.message')}</p>
      <button
        type='button'
        onClick={handleBackButtonClick}
        className='btn btn-outline-primary'>
        <FontAwesomeIcon icon={Icons.faArrowCircleLeft} /> &nbsp;
        {t('contact.form.submit.feedback.success.button')}
      </button>
    </div>
  )

  const contactFormSubmitError = (
    <div className='alert alert-dismissible alert-danger'>
      <p className='h5 text-danger'>{t('contact.form.submit.feedback.error.message')}</p>
      <button
        type='button'
        onClick={ handleBackButtonClick }
        className='btn btn-outline-danger'>
        <FontAwesomeIcon icon={Icons.faArrowCircleLeft} /> &nbsp;
        {t('contact.form.submit.feedback.error.button')}
      </button>
    </div>
  )

  const contactForm = (
    <div>
      <h1 className='h1 text-light'>{t('contact.title')}</h1>
      <h2 className='h4 text-light'>{t('contact.hook')}</h2>
      <form onSubmit={handleSubmit(handleAfterSubmit)}>
        <div className='form-group'>
          <label className='col-form-label text-light'>
            {t('contact.form.name.label')}
          </label>
          <input
            name='name'
            type='text'
            ref={register}
            className={
              errors.name
                ? 'form-control text-danger is-invalid'
                : formState.dirtyFields.has('name')
                ? 'form-control is-valid'
                : 'form-control'
            }
            placeholder={t('contact.form.name.placeholder')}
          />
          {errors.name && (
            <div className='invalid-feedback'>{errors.name.message}</div>
          )}
        </div>
        <div className='form-group'>
          <label className='col-form-label text-light'>
            {t('contact.form.email.label')}
          </label>
          <input
            name='email'
            type='text'
            ref={register}
            placeholder={t('contact.form.email.placeholder')}
            className={
              errors.email
                ? 'form-control text-danger is-invalid'
                : formState.dirtyFields.has('email')
                ? 'form-control is-valid'
                : 'form-control'
            }
          />
          {errors.email && (
            <div className='invalid-feedback'>{errors.email.message}</div>
          )}
        </div>
        <div className='form-group'>
          <label className='col-form-label text-light'>
            {t('contact.form.phone.label')}
          </label>
          <Controller
            name='phone'
            as={PhoneInput}
            control={control}
            inputClass={
              errors.phone
                ? 'form-control text-danger is-invalid'
                : formState.dirtyFields.has('phone')
                ? 'form-control is-valid'
                : 'form-control'
            }
            country='fr'
            preferredCountries={['fr', 'us', 'ch', 'de', 'lu', 'gb']}
          />
        </div>
        {errors.phone && (
          <div className='invalid-feedback'>{errors.phone.message}</div>
        )}
        <div className='form-group text-light'>
          <label>{t('contact.form.message.label')}</label>
          <textarea
            name='message'
            ref={register}
            placeholder={t('contact.form.message.placeholder')}
            rows='5'
            className={
              errors.message
                ? 'form-control text-danger is-invalid'
                : formState.dirtyFields.has('message')
                ? 'form-control is-valid'
                : 'form-control'
            }
          />
          {errors.message && (
            <div className='invalid-feedback'>{errors.message.message}</div>
          )}
        </div>
        <div id='contact-form-interactive-container'>
          <button type='submit' className='btn btn-primary btn-block'>
            <div>
              <FontAwesomeIcon icon={Icons.faPaperPlane} /> &nbsp;
              {t('contact.form.submit.button')}
            </div>
          </button>
        </div>
      </form>
    </div>
  )
  
  return (
    <div className='screen vertical-center background-skyline'>
      <div className='container'>
        <div className='row'>
          <div className='col-12'>
            {
              formStatus === 'error'
              ? contactFormSubmitError
                : formStatus === 'success'
                ? contactFormSubmitSuccess
              : contactForm
              }
          </div>
        </div>
      </div>
    </div>
  )
}

export default ContactScreen