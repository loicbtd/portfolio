import 'bootswatch/dist/lux/bootstrap.min.css'
import 'vendor/fontawesome/css/all.css'

import React, { Component } from 'react'

import { Carousel } from 'react-bootstrap'
import project_android_agenda from 'resources/images/project-android-agenda.webp'
import project_android_foodwatcher from 'resources/images/project-android-foodwatcher.webp'
import project_java_assassinsgrid from 'resources/images/project-java-assassinsgrid.webp'
import project_multi_imr from 'resources/images/project-multi-imr.webp'
import project_multi_relaxtravel from 'resources/images/project-multi-relaxtravel.webp'
import project_php_librarymanager from 'resources/images/project-php-librarymanager.webp'
import project_php_youtube from 'resources/images/project-php-youtube.webp'
import project_unrealengine_needforracing from 'resources/images/project-unrealengine-needforracing.webp'
import { withTranslation } from 'react-i18next'

class ProjectsScreen extends Component {
  render() {
    const t = this.props.t
    return (
      <Carousel interval={5000} className='screen background-teamwork'>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='project-smartphone img-not-draggable'
                  src={project_multi_relaxtravel}
                  alt={t('projects.multi_relaxtravel.title')}
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h2 text-light'>
                  {t('projects.multi_relaxtravel.title')}
                </h1>
                <p className='slide-text-content text-light'>
                  {t('projects.multi_relaxtravel.content')}
                </p>
                <a
                  href={t('projects.multi_relaxtravel.links.code')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-gitlab'></i> &nbsp;
                  {t('projects.button.code')}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='project-smartphone img-not-draggable'
                  src={project_multi_imr}
                  alt={t('projects.multi_imr.title')}
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h2 text-light'>
                  {t('projects.multi_imr.title')}
                </h1>
                <p className='slide-text-content text-light'>
                  {t('projects.multi_imr.content')}
                </p>
                <a
                  href={t('projects.multi_imr.links.code')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-github'></i> &nbsp;
                  {t('projects.button.code')}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='project-smartphone img-not-draggable'
                  src={project_android_agenda}
                  alt={t('projects.android_agenda.title')}
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h2 text-light'>
                  {t('projects.android_agenda.title')}
                </h1>
                <p className='slide-text-content text-light'>
                  {t('projects.android_agenda.content')}
                </p>
                <a
                  href={t('projects.android_agenda.links.demo')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-youtube'></i> &nbsp;
                  {t('projects.button.demo')}
                </a>
                <a
                  href={t('projects.android_agenda.links.code')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-github'></i> &nbsp;
                  {t('projects.button.code')}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='project-smartphone img-not-draggable'
                  src={project_android_foodwatcher}
                  alt={t('projects.android_foodwatcher.title')}
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h2 text-light'>
                  {t('projects.android_foodwatcher.title')}
                </h1>
                <p className='slide-text-content text-light'>
                  {t('projects.android_foodwatcher.content')}
                </p>
                <a
                  href={t('projects.android_foodwatcher.links.demo')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-youtube'></i> &nbsp;
                  {t('projects.button.demo')}
                </a>
                <a
                  href={t('projects.android_foodwatcher.links.code')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-github'></i> &nbsp;
                  {t('projects.button.code')}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='project-laptop img-not-draggable'
                  src={project_java_assassinsgrid}
                  alt={t('projects.java_assassinsgrid.title')}
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h2 text-light'>
                  {t('projects.java_assassinsgrid.title')}
                </h1>
                <p className='slide-text-content text-light'>
                  {t('projects.java_assassinsgrid.content')}
                </p>
                <a
                  href={t('projects.java_assassinsgrid.links.demo')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-youtube'></i> &nbsp;
                  {t('projects.button.demo')}
                </a>
                <a
                  href={t('projects.java_assassinsgrid.links.code')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-gitlab'></i> &nbsp;
                  {t('projects.button.code')}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='project-laptop img-not-draggable'
                  src={project_php_librarymanager}
                  alt={t('projects.php_librarymanager.title')}
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h2 text-light'>
                  {t('projects.php_librarymanager.title')}
                </h1>
                <p className='slide-text-content text-light'>
                  {t('projects.php_librarymanager.content')}
                </p>
                <a
                  href={t('projects.php_librarymanager.links.demo')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fas fa-mouse-pointer'></i> &nbsp;
                  {t('projects.button.demo')}
                </a>
                <a
                  href={t('projects.php_librarymanager.links.code')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-gitlab'></i> &nbsp;
                  {t('projects.button.code')}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='project-laptop img-not-draggable'
                  src={project_php_youtube}
                  alt={t('projects.php_youtube.title')}
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h2 text-light'>
                  {t('projects.php_youtube.title')}
                </h1>
                <p className='slide-text-content text-light'>
                  {t('projects.php_youtube.content')}
                </p>
                <a
                  href={t('projects.php_youtube.links.demo')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fas fa-mouse-pointer'></i> &nbsp;
                  {t('projects.button.demo')}
                </a>
                <a
                  href={t('projects.php_youtube.links.code')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-gitlab'></i> &nbsp;
                  {t('projects.button.code')}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className='carousel-item'>
          <div className='container'>
            <div className='row screen screen vertical-center'>
              <div className='col-12 col-md-4 text-center'>
                <img
                  className='project-laptop img-not-draggable'
                  src={project_unrealengine_needforracing}
                  alt={t('projects.unrealengine_needforracing.title')}
                />
              </div>
              <div className='col-12 col-md-8'>
                <h1 className='h2 text-light'>
                  {t('projects.unrealengine_needforracing.title')}
                </h1>
                <p className='slide-text-content text-light'>
                  {t('projects.unrealengine_needforracing.content')}
                </p>
                <a
                  href={t('projects.unrealengine_needforracing.links.demo')}
                  className='btn btn-secondary m-1'
                  target='_blank'
                  rel='noopener noreferrer'>
                  <i className='fab fa-youtube'></i> &nbsp;
                  {t('projects.button.demo')}
                </a>
              </div>
            </div>
          </div>
        </div>
      </Carousel>
    )
  }
}

export default withTranslation()(ProjectsScreen)