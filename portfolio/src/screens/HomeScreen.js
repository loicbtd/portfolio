import 'bootswatch/dist/lux/bootstrap.min.css'

import * as Icons from '@fortawesome/free-solid-svg-icons'
import * as Three from 'three'

import React, { Component } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-scroll'
import TextLoop from 'react-text-loop'
import VantaNetwork from 'vanta/dist/vanta.net.min'
import brand_loicbertrand_light_full from 'resources/images/brand-loicbertrand-light-full.webp'
import i18n from 'resources/languages/i18n'
import { withTranslation } from 'react-i18next'

class HomeScreen extends Component {
  componentDidMount() {
    this.animatedNetworkBackground = VantaNetwork({
      el: this.animatedNetworkBackground.current,
      THREE: Three,
      mouseControls: true,
      touchControls: true,
      minHeight: 200.0,
      minWidth: 200.0,
      scale: 1.0,
      scaleMobile: 1.0,
      color: 0xffffff,
      backgroundColor: 0x0,
      points: 4.5,
      maxDistance: 22.0,
      spacing: 17.5,
    })
  }

  componentWillUnmount() {
    if (this.animatedNetworkBackground) {
      this.animatedNetworkBackground.destroy()
    }
  }
  changeLanguage(language) {
    i18n.changeLanguage(language)
  }

  render() {
    const t = this.props.t
    return (
      <div
        ref={(this.animatedNetworkBackground = React.createRef())}
        className='screen vertical-center'>
        <div className='language-button-group'>
          <button
            className='btn btn-outline-secondary btn-inline-block m-1'
            onClick={() => this.changeLanguage('fr')}>
            fr
          </button>
          <button
            className='btn btn-outline-secondary btn-inline-block m-1'
            onClick={() => this.changeLanguage('en')}>
            en
          </button>
        </div>
        <div className='brand-container'>
          <img
            src={brand_loicbertrand_light_full}
            alt='Loïc BERTRAND Light Logo Full'
            height='100'
            rel='noopener noreferrer'
            className='img-not-draggable brand-img'
          />
        </div>
        <div className='social-container'>
          <a
            href='https://www.linkedin.com/in/loicbtd'
            target='_blank'
            rel='noopener noreferrer'
            className='social-link'>
            <i className='fab fa-linkedin'></i>
          </a>
          <a
            href='mailto:loic.bert.marcel@gmail.com'
            target='_blank'
            rel='noopener noreferrer'
            className='social-link'>
            <i className='fas fa-envelope'></i>
          </a>
          <a
            href='https://github.com/loicbtd'
            target='_blank'
            rel='noopener noreferrer'
            className='social-link'>
            <i className='fab fa-github'></i>
          </a>
          <a
            href='https://gitlab.com/loicbtd'
            target='_blank'
            rel='noopener noreferrer'
            className='social-link'>
            <i className='fab fa-gitlab'></i>
          </a>
          <a
            href='https://twitter.com/loicbert'
            target='_blank'
            rel='noopener noreferrer'
            className='social-link'>
            <i className='fab fa-twitter'></i>
          </a>
        </div>
        <div className='container'>
          <div className='row vertical-center'>
            <div className='col-12 col-md-6'>
              <h1 className='text-white inline display-4'>
                {t('home.welome_word')}
              </h1>
              <h2 className='text-white'>{t('home.lets_discover')}</h2>
              <h3 className='text-white'>
                <TextLoop
                  children={[
                    t('home.text_loop.me'),
                    t('home.text_loop.projects'),
                    t('home.text_loop.knowledges'),
                    t('home.text_loop.contact'),
                  ]}
                />
              </h3>
            </div>
            <nav className='col-12 col-md-6 text-center'>
              <Link
                to='me'
                smooth={true}
                duration={1000}
                className='btn btn-secondary btn-inline-block btn-block m-1'>
                <FontAwesomeIcon icon={Icons.faAddressCard} /> &nbsp;
                {t('home.nav.me')}
              </Link>
              <Link
                to='knowledges'
                smooth={true}
                duration={1000}
                className='btn btn-secondary btn-inline-block btn-block m-1'>
                <FontAwesomeIcon icon={Icons.faBriefcase} /> &nbsp;
                {t('home.nav.knowledges')}
              </Link>
              <Link
                activeClass='active'
                to='projects'
                smooth={true}
                duration={1000}
                className='btn btn-secondary btn-inline-block btn-block m-1'>
                <FontAwesomeIcon icon={Icons.faFlagCheckered} /> &nbsp;
                {t('home.nav.projects')}
              </Link>
              <Link
                activeClass='active'
                to='contact'
                smooth={true}
                duration={1000}
                className='btn btn-secondary btn-inline-block btn-block m-1'>
                <FontAwesomeIcon icon={Icons.faEnvelope} /> &nbsp;{' '}
                {t('home.nav.contact')}
              </Link>
            </nav>
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation()(HomeScreen)