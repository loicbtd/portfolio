import 'bootswatch/dist/lux/bootstrap.min.css'
import 'resources/style/App.css'

import { Element, animateScroll, scrollSpy } from 'react-scroll'
import React, { Component, Suspense } from 'react'

import ContactScreen from 'screens/ContactScreen'
import Fab from 'components/Fab'
import HomeScreen from 'screens/HomeScreen'
import { Howl } from 'howler'
import KnowledgesScreen from 'screens/KnowledgesScreen'
import MeScreen from 'screens/MeScreen'
import ProjectsScreen from 'screens/ProjectsScreen'
import { Spinner } from 'react-bootstrap'
import prelude_and_fugue from 'resources/audio/prelude_and_fugue.mp3'

class App extends Component {
  componentDidMount() {
    this.sound = new Howl({
      src: prelude_and_fugue,
      loop: true,
      volume: 0.5,
    })

    this.toggleSound()

    scrollSpy.update()
  }

  scrollToTop() {
    animateScroll.scrollToTop()
  }

  toggleSound() {
    if (this.sound.playing()) {
      this.sound.pause()
    } else {
      this.sound.play()
    }
  }

  render() {
    const Loader = () => (
      <Spinner animation='border' role='status'></Spinner>
    )

    return (
      <Suspense fallback={<Loader />}>
        <Fab parent={this}></Fab>
        <Element name='home' className='element'>
          <HomeScreen />
        </Element>
        <Element id='me' name='me' className='element'>
          <MeScreen />
        </Element>
        <Element name='knowledges' className='element'>
          <KnowledgesScreen />
        </Element>
        <Element name='projects' className='element'>
          <ProjectsScreen />
        </Element>
        <Element name='contact' className='element'>
          <ContactScreen parent={this} />
        </Element>
      </Suspense>
    )
  }
}

export default App