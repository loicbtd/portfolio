import './Fab.css'
import 'vendor/animate/animate.css'
import 'vendor/fontawesome/css/all.css'

import React, { Component } from 'react'

class Fab extends Component {
  constructor(props) {
    super(props)
    this.state = { isDisplayed: false }
    this.handleScroll = this.handleScroll.bind(this)
    this.handleFabClick = this.handleFabClick.bind(this)
    this.handleDocumentClick = this.handleDocumentClick.bind(this)
  }

  render() {
    if (!this.state.isDisplayed) {
      return <div></div>
    }
    return (
      <div id='fa-container'>
        <div id='fa-inner'>
          <button id='fa-button'>
            <i className='fas fa-ellipsis-v'></i>
          </button>
          <ul id='fa-menu'>
            <li>
              <button id='volume-button' className='fa-item'>
                <i id='volume-icon' className='fas fa-volume-mute'></i>
              </button>
            </li>
            <li>
              <button id='tothetop-button' className='fa-item'>
                <i className='fas fa-home'></i>
              </button>
            </li>
          </ul>
        </div>
      </div>
    )
  }

  componentDidMount() {
    if (this.props.startToDisplayAfter) {
      let id = this.props.startToDisplayAfter
      let element = document.getElementById(id)
      if (element) {
        window.addEventListener('scroll', this.handleScroll)
        this.handleScroll()
      }
    }
    else {
      this.setState({ isDisplayed: true })
    }
  }

  componentDidUpdate() {
    if (this.state.isDisplayed) {
      document.addEventListener('click', this.handleDocumentClick)
      
      document
        .getElementById('fa-button')
        .addEventListener('click', this.handleFabClick)
      
      Array.from(document.getElementsByClassName('fa-item')).forEach(
        (element) => {
          element.addEventListener('click', this.handleFabClick)
        }
      )
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleDocumentClick)
  }

  handleScroll() {
    let scrollPositionY = 0

    if (window instanceof window.Window) {
      scrollPositionY = window.scrollY
    } else {
      scrollPositionY = window.scrollTop
    }

    let id = this.props.startToDisplayAfter
    let element = document.getElementById(id)
    if (!element) return
    let elementPosition = element.offsetTop

    if (scrollPositionY >= elementPosition && !this.state.isDisplayed) {
      this.setState({ isDisplayed: true })
    }
    if (scrollPositionY < elementPosition && this.state.isDisplayed) {
      this.setState({ isDisplayed: false })
    }
  }

  handleFabClick(event) {
    event.preventDefault()
    let target = event.target
    if (target === document.getElementById('volume-button') ) {
      this.props.parent.toggleSound()
      if (this.props.parent.sound.playing()) {
        document.getElementById('volume-icon').className = 'fas fa-volume-mute'
      } else {
        document.getElementById('volume-icon').className = 'fas fa-volume-up'
      }
    }
    
    if (target === document.getElementById('tothetop-button')) {
      this.props.parent.scrollToTop()
    }

    this.toggleFab()
  }

  handleDocumentClick(event) {
    let target = event.target
    if (document.getElementById('fa-button').classList.contains('open')) {
      if (target.id !== 'fa-button' && !target.classList.contains('fa-item')) {
        this.toggleFab()
      }
    }
  }

  toggleFab() {
    document.getElementById('fa-button').classList.toggle('open')
    if (document.getElementById('fa-button').classList.contains('open')) {
      document.getElementById('fa-menu').classList = 'animated slideInRight'
      document.getElementById('fa-menu').style.display = 'block'
    } else {
      document.getElementById('fa-menu').classList = 'animated slideOutRight'      
    }
  }
}

export default Fab