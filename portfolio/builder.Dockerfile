FROM node:13.13.0-alpine

ARG REACT_APP_EMAILJS_UID
ARG REACT_APP_EMAILJS_SERVICEID
ARG REACT_APP_EMAILJS_TEMPLATEID

COPY . /container/app
ENV PATH=/container/app/node_modules/.bin:$PATH
WORKDIR /container/app
RUN npm install --silent
RUN npm run build